package pe.edu.pucp.taxicab;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ViewHolder>{

    private List<String> items = new ArrayList<>();

    public ItemAdapter(List<String> items) {
        this.items = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewRoot = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rv, parent, false);
        return new ViewHolder(viewRoot);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mCounter.setText("" + position);
        holder.mLabel.setText(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    // ViewHolder
    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mCounter, mLabel;

        public ViewHolder(View itemView) {
            super(itemView);
            mCounter = itemView.findViewById(R.id.counter);
            mLabel = itemView.findViewById(R.id.counter_label);
        }
    }
}
